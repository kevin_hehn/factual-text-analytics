package de.uni.mannheim.fta.utils;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.uni.mannheim.fta.processing.ProcessXML;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class Utils {
    // Returns empty string if the object is null
    public static String checkNull(Object obj){
        if(obj == null){
            return "";
        } else {
            return obj.toString();
        }
    }
    
    // Get all the sub-directories recursively
    public static List<File> getSubdirs(File file) {
        List<File> subdirs = Arrays.asList(file.listFiles(new FileFilter() {
            public boolean accept(File f) {
                return f.isDirectory();
            }
        }));
        subdirs = new ArrayList<File>(subdirs);

        List<File> deepSubdirs = new ArrayList<File>();
        for(File subdir : subdirs) {
            deepSubdirs.addAll(getSubdirs(subdir)); 
        }
        
        subdirs.addAll(deepSubdirs);
        
        return subdirs;
    }
    
    // Get all the leaf sub-directories
    public static List<File> getLeafSubdirs(File file){
        List<File> leafSubdirs = new ArrayList<File>();
        List<File> allSubdirs = Utils.getSubdirs(file);
        
        for (File f: allSubdirs){
            File [] subfiles = f.listFiles();
            File [] subSubFiles = subfiles[0].listFiles();
            if (subSubFiles == null) leafSubdirs.add(f);
        }
        
        return leafSubdirs;
    }
}