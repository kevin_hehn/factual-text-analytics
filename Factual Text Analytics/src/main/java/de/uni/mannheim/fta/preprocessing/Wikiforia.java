package de.uni.mannheim.fta.preprocessing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import se.lth.cs.nlp.wikiforia.App;

/**
 * This class does the Wikiforia processing from the raw xml dump. This can be
 * done locally on a reasonable machine, no cluster needed.
 * 
 * @author Kevin
 *
 */
public class Wikiforia {

	private String directoryName;
	private String directoryNameSplit;

	/**
	 * This starts the Wikiforia extraction. Please adapt the parameters
	 * according to where you located the files.
	 * 
	 * Errors due to sweble error - They can be ignored:
	 * https://en.wikipedia.org/wiki/Dynamic_programming
	 * https://de.wikipedia.org/wiki/Hilfe:Tags#pre
	 * https://github.com/sweble/sweble-wikitext/issues/43
	 * https://github.com/sweble/sweble-wikitext/issues/35
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Wikiforia test = new Wikiforia();
		/*
		 * Step 1: Please locate the Wikipedia Dump Files in the input folder
		 * There should be two files:
		 * enwiki-20170201-pages-articles-multistream.xml.bz2 &
		 * enwiki-20170201-pages-articles-multistream-index.txt.bz2
		 */
		test.directoryName = "E:\\ENWIKI\\OriginalData";
		test.directoryNameSplit = "E:\\ENWIKI\\OriginalData\\Splitted2";

		/*
		 * STEP 2: Remove the Wikipedia specific annotation using Wikiforia.
		 */
		test.convertWikiforiaTextFiles();

		/*
		 * STEP 3: Remove the linefeed from the Wikiforia extract. Also adding
		 * [##|##] for each paragraph You can leave these unchanged
		 */
		String inputFile = "\\wikipedia_wikiforia_step_1.xml";
		String outputFile = "\\wikipedia_wikiforia_step_2.xml";
		test.removeLineFeed(inputFile, outputFile);

		/*
		 * STEP 4: Split the output from STEP 3 into packages of 1000 articles.
		 */
		ReadFileByLine.splitByLine(test.directoryName + outputFile, test.directoryNameSplit);
	}

	/**
	 * Starts the Wikiforia extraction.
	 * Refers to: https://github.com/marcusklang/wikiforia
	 */
	public void convertWikiforiaTextFiles() {

		String[] command = new String[8];

		command[0] = "-pages";
		command[1] = directoryName + "\\enwiki-20170201-pages-articles-multistream.xml.bz2";
		command[2] = "-output";
		command[3] = directoryName + "\\wikipedia_wikiforia_step_1.xml";
		command[4] = "-lang";
		command[5] = "en";
		// namespace 0 are articles => Just include them
		command[6] = "-fns";
		command[7] = "0";

		App.main(command);
	}

	/**
	 * Removes the LF of the Wikipedia XML and inserts [##|##] for when a
	 * paragraph occurs
	 * 
	 * @param input
	 * @param output
	 */
	public void removeLineFeed(String input, String output) {

		File file = new File(this.directoryName + input);
		FileInputStream fis = null;
		BufferedWriter bw = null;
		FileWriter fw = null;
		BufferedReader in = null;
		try {

			File outputfile = new File(this.directoryName + output);
			outputfile.delete();
			// if file doesnt exists, then create it
			if (!outputfile.exists()) {
				outputfile.createNewFile();
			}

			// true = append file

			// fw = new FileWriter(outputfile.getAbsoluteFile(), true);
			bw = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(outputfile.getAbsoluteFile()), "UTF-8"));

			fis = new FileInputStream(file);
			in = new BufferedReader(new InputStreamReader(fis, "UTF-8"));

			System.out.println("Total file size to read (in bytes) : " + fis.available());

			int content;
			// Skip the Header
			fis.skip(46);
			int counter = 0;
			int lfcounter = 0;
			while ((content = in.read()) != -1) {
				// convert to char and display it
				if (content == (int) '\n') {
					lfcounter++;
					// Just a paragraph, if there a two subsequent LFs.
					if (lfcounter > 1) {
						String tempString = "[##|##]";
						bw.write(tempString);
						lfcounter = 0;
					}
					// Recognize if there is a </page> tag, ending a article, has to be done like
					// this since it is read char wise
				} else if (content == (int) '<' && counter == 0) {
					counter++;
					lfcounter = 0;
					char temp = (char) content;
					String tempString = Character.toString(temp);
					bw.write(tempString);
				} else if (content == (int) '/' && counter == 1) {
					counter++;
					lfcounter = 0;
					char temp = (char) content;
					String tempString = Character.toString(temp);
					bw.write(tempString);
				} else if (content == (int) 'p' && counter == 2) {
					counter++;
					lfcounter = 0;
					char temp = (char) content;
					String tempString = Character.toString(temp);
					bw.write(tempString);
				} else if (content == (int) 'a' && counter == 3) {
					counter++;
					lfcounter = 0;
					char temp = (char) content;
					String tempString = Character.toString(temp);
					bw.write(tempString);
				} else if (content == (int) 'g' && counter == 4) {
					counter++;
					lfcounter = 0;
					char temp = (char) content;
					String tempString = Character.toString(temp);
					bw.write(tempString);
				} else if (content == (int) 'e' && counter == 5) {
					counter++;
					lfcounter = 0;
					char temp = (char) content;
					String tempString = Character.toString(temp);
					bw.write(tempString);
				} else if (content == (int) '>' && counter == 6) {
					counter = 0;
					lfcounter = 0;
					char temp = (char) content;
					String tempString = Character.toString(temp);
					bw.write(tempString);
					tempString = "\n";
					bw.write(tempString);

				} else {
					char temp = (char) content;
					String tempString = Character.toString(temp);
					bw.write(tempString);
					counter = 0;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fis != null)
					fis.close();
				if (bw != null)
					bw.close();
				if (in != null)
					in.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

}
