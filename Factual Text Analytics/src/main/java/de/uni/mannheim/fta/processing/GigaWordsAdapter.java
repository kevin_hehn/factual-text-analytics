package de.uni.mannheim.fta.processing;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * Adapter Class for the GigaWords Corpus
 * 
 * @author Kevin
 * 
 */
public class GigaWordsAdapter {

	// Static Array to store the different articles
	public ArrayList<RawGigaWordArticle> articles = new ArrayList<RawGigaWordArticle>();

	public GigaWordsAdapter() {
		articles = new ArrayList<RawGigaWordArticle>();
	}

	public ArrayList<RawGigaWordArticle> getArticleList() {
		return this.articles;
	}

	/**
	 * This method starts the parsing of an document. Since the gigaword files
	 * are not in a well formatted xml schema, we need to wrap a root element
	 * around the data, which is done by a concatenation of strings to the
	 * content string. Then the content is well formatted and can be processed
	 * further using a XML SAX Parser.
	 * 
	 * @param content
	 * @param i
	 */
	public void startAdapter(String content, int i) {
		try {
			parseXML(("<root>" + content + "</root>").replaceAll("&AMP", "&amp"), i);
		} catch (SAXException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * This is actually used for the processing. Basically this method is setting
	 * up a XMLReader together with a custom content handler. Using this handler
	 * it will extract the necessary information from the content string and
	 * will create a java object out of it.
	 * 
	 * @param content
	 * @param i
	 * @throws SAXException
	 * @throws IOException
	 */
	public void parseXML(String content, int i) throws SAXException, IOException {
		XMLReader xmlReader = XMLReaderFactory.createXMLReader();
		GigaWordsHandler handler = new GigaWordsHandler();
		xmlReader.setContentHandler(handler);
		InputSource inputSource = new InputSource(new ByteArrayInputStream(content.getBytes()));
		xmlReader.parse(inputSource);
		this.articles = handler.getArticles();
	}
}
