package de.uni.mannheim.fta.processing;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

/**
 * SAX Handler for parsing the GigaWords XML Files
 * 
 * @author Kevin
 *
 */
public class GigaWordsHandler implements ContentHandler {
	private ArrayList<RawGigaWordArticle> articles = new ArrayList<RawGigaWordArticle>();
	private String currentValue;
	StringBuilder sb = new StringBuilder();
	RawGigaWordArticle art = null;

	/**
	 * This method is called by the sax parser, if new Text is found
	 */
	public void characters(char[] ch, int start, int length) throws SAXException {
		currentValue = new String(ch, start, length);
		// currentValue = currentValue.trim();
		sb.append(currentValue);

	}

	/**
	 * This method is called, if the SAX Parser finds an start tag.
	 */
	public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
		// Create a new GigaWordArticle
		if (localName.equals("DOC")) {
			art = new RawGigaWordArticle();
			art.setDocumentID(atts.getValue("id"));
			art.setDocumentType(atts.getValue("type"));
		}

	}

	/**
	 * This method is called if the sax parser finds an end tag
	 */
	public void endElement(String uri, String localName, String qName) throws SAXException {
		// There can be multiple documents in one file. Therefore every time the
		// process finds a DOC end
		// tag it will add the current article to the ArrayList and will null
		// the art variable afterwards
		if (localName.equals("DOC")) {
			this.articles.add(art);
			art = null;
		}
		if (localName.equals("TEXT")) {
			art.setFullText(sb.toString().trim());
			sb = new StringBuilder();
		}
		if (localName.equals("P")) {
			sb.append("[##|##]");
		}

		if (localName.equals("HEADLINE")) {
			art.setHeadline(sb.toString().trim());
			sb = new StringBuilder();
		}
		if (localName.equals("DATELINE")) {
			art.setDateline(sb.toString().trim());
			sb = new StringBuilder();
		}
	}

	public ArrayList<RawGigaWordArticle> getArticles() {
		return this.articles;
	}

	public void endDocument() throws SAXException {
	}

	public void endPrefixMapping(String prefix) throws SAXException {
	}

	public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
	}

	public void processingInstruction(String target, String data) throws SAXException {
	}

	public void setDocumentLocator(Locator locator) {
	}

	public void skippedEntity(String name) throws SAXException {
	}

	public void startDocument() throws SAXException {
	}

	public void startPrefixMapping(String prefix, String uri) throws SAXException {
	}
}
