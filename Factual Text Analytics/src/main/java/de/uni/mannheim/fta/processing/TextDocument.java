package de.uni.mannheim.fta.processing;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import avroschema.Sentence;
import avroschema.Span;
import avroschema.Token;
import de.uni.mannheim.fta.utils.CoreNLPUtils;
import edu.stanford.nlp.coref.CorefCoreAnnotations.CorefChainAnnotation;
import edu.stanford.nlp.coref.data.CorefChain;
import edu.stanford.nlp.coref.data.CorefChain.CorefMention;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.BasicDependenciesAnnotation;
import edu.stanford.nlp.util.CoreMap;

/**
 * Given a input text string, this class can be used to create annotated data.
 * For annotating the Stanford Core NLP tools are used.
 * 
 * @author Kiril Gashteovski, Kevin
 *
 */
public class TextDocument {

	// Declaration
	private String text;
	private static final Logger logger = LogManager.getLogger("TextDocument Details Logger");
	private StanfordCoreNLP pipeline;
	private java.util.Map<String, String> repMentions = new java.util.HashMap<String, String>();
	private java.util.Map<String, Integer> repMentionsSentences = new java.util.HashMap<String, Integer>();
	private java.util.HashSet<java.lang.String> locations;
	private java.util.HashSet<java.lang.String> people;
	private java.util.HashSet<java.lang.String> organizations;

	// Each document consists of a list of sentences, structured according
	// to the AVRO schema of a sentence
	private List<Sentence> sentences;

	/**
	 * Constructor Sets the input text, as well as the CoreNLP pipeline.
	 * 
	 * @param text
	 * @param pipeline
	 */
	public TextDocument(String text, StanfordCoreNLP pipeline) {
		this.text = text;
		this.pipeline = pipeline;
		this.repMentions = new java.util.HashMap<String, String>();
		this.locations = new java.util.HashSet<java.lang.String>();
		this.people = new java.util.HashSet<java.lang.String>();
		this.organizations = new java.util.HashSet<java.lang.String>();
		this.repMentionsSentences = new java.util.HashMap<String, Integer>();
	}

	public void setPipeline(StanfordCoreNLP pipeline) {
		this.pipeline = pipeline;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<Sentence> getSentences(int sentenceOffset) {
		Annotation document = null;
		try {
			document = new Annotation(this.text);
			this.pipeline.annotate(document);
		} catch (Exception e) {
			logger.debug("There is a Problem with the annotation of the article");
			logger.debug("*****************************************************");
			logger.debug(this.text);
			logger.debug("*****************************************************");
			logger.debug(e.getStackTrace());
			return this.sentences = new ArrayList<Sentence>();
		}
		ArrayList<CoreferenceOccurence> occurences = new ArrayList<CoreferenceOccurence>();

		Map<Integer, CorefChain> coref = document.get(CorefChainAnnotation.class);
		for (Map.Entry<Integer, CorefChain> entry : coref.entrySet()) {
			CorefChain c = entry.getValue();
			if (repMentions.get(Integer.toString(c.getChainID())) == null) {
				repMentions.put(Integer.toString(c.getChainID()), c.getRepresentativeMention().mentionSpan);
			}
			if (repMentionsSentences.get(Integer.toString(c.getChainID())) == null)
				// Sentence indexing starts from one. For us is starts with 0.
				// Therefore decrease by one!
				repMentionsSentences.put(Integer.toString(c.getChainID()), c.getRepresentativeMention().sentNum - 1);
			List<CorefMention> mentions = c.getMentionsInTextualOrder();
			for (CorefMention m : mentions) {

				CoreferenceOccurence occurence = new CoreferenceOccurence();
				String[] toSplit = c.getRepresentativeMention().toString().split("\"");
				occurence.setRepresentativeMention(toSplit[1]);
				occurence.setSentenceNumber(m.position.get(0));
				occurence.setStartIndex(m.startIndex);
				occurence.setEndIndex(m.endIndex);
				occurence.setClusterID(m.corefClusterID);
				occurences.add(occurence);
			}
		}

		// Get the sentences from a text in both CoreMap and Sentence lists
		List<CoreMap> coreMapSentences = document.get(SentencesAnnotation.class);

		this.sentences = new ArrayList<Sentence>();

		// For each sentence, add the fields for the schema (POS, NER, DP, ... )
		int sentID = sentenceOffset;

		int sentStartIndex = 0;
		int sentEndIndex = 0;
		int sentenceIndex = 1;
		int tokenIndex = 1;
		for (CoreMap sentence : coreMapSentences) {
			// Get the tokens
			List<CoreLabel> tokens = sentence.get(TokensAnnotation.class);
			List<Token> toks = new ArrayList<Token>();

			// For each token, add NEs, POS, span
			int lastEndIndex = 0;
			String lastTag = "";
			String buildNERString = "";
			int tokIndex = 0;
			for (CoreLabel t : tokens) {
				Token tok = new Token();
				boolean isChain = false;
				tok.setLemma(t.lemma());
				tok.setWord(t.word());
				tok.setNer(t.ner());
				tok.setPos(t.tag());
				tok.setIndex(t.index());
				Span s = new Span();
				s.setStartIndex(t.beginPosition() - sentEndIndex);
				s.setEndIndex(t.endPosition() - sentEndIndex);
				lastEndIndex = t.endPosition() - sentEndIndex;
				tok.setSpan(s);
				tok.setChainId("");
				/*
				 * Problem: If e.g a person name consists of multiple tokens, we
				 * don't want to add each token, but the whole string.
				 * 
				 */
				if ((t.ner().equals("LOCATION") || t.ner().equals("PERSON") || t.ner().equals("ORGANIZATION"))) {
					buildNERString = buildNERString + " " + t.word();
					isChain = true;
				} else {
					isChain = false;
				}

				if ((isChain == false && buildNERString != "")
						|| (isChain == true && tokens.size() == tokIndex + 1 && buildNERString != "")) {
					if (lastTag.equals("LOCATION"))
						locations.add(buildNERString.trim());
					if (lastTag.equals("PERSON"))
						people.add(buildNERString.trim());
					if (lastTag.equals("ORGANIZATION"))
						organizations.add(buildNERString.trim());

					buildNERString = "";
				}
				lastTag = t.ner();

				tokIndex++;

				for (CoreferenceOccurence oc : occurences) {
					if (oc.getSentenceNumber() == sentenceIndex && tokenIndex >= oc.getStartIndex()
							&& tokenIndex < oc.getEndIndex()) {
						tok.setChainId(Integer.toString(oc.getClusterID()));
					}
				}
				toks.add(tok);
				tokenIndex++;
			}
			sentEndIndex += lastEndIndex;

			// Parse the sentence
			SemanticGraph semanticGraph = sentence.get(BasicDependenciesAnnotation.class);

			// Add sentence ID, the typed dependencies of the sentence and the
			// semantic graph
			Sentence sent = new Sentence();
			Span sentSpan = new Span();
			sentSpan.setStartIndex(sentStartIndex);
			sentStartIndex = sentEndIndex + 1;
			sentSpan.setEndIndex(sentEndIndex);
			sentEndIndex++;
			sent.setSpan(sentSpan);
			sent.setTokens(toks);
			sent.setSId(sentID);
			sent.setSg(semanticGraph.toCompactString());
			sent.setDp(semanticGraph.typedDependencies().toString());
			sentID++;

			// Add the processed sentence to the list of sentences
			this.sentences.add(sent);
			tokenIndex = 1;
			sentenceIndex++;
		}

		return this.sentences;
	}

	public java.util.HashSet<java.lang.String> getLocations() {
		return locations;
	}

	public void setLocations(java.util.HashSet<java.lang.String> locations) {
		this.locations = locations;
	}

	public java.util.HashSet<java.lang.String> getPeople() {
		return people;
	}

	public void setPeople(java.util.HashSet<java.lang.String> people) {
		this.people = people;
	}

	public java.util.HashSet<java.lang.String> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(java.util.HashSet<java.lang.String> organizations) {
		this.organizations = organizations;
	}

	public java.util.Map<String, String> getRepMentions() {
		return repMentions;
	}

	public void setRepMentions(java.util.Map<String, String> repMentions) {
		this.repMentions = repMentions;
	}

	public java.util.Map<String, Integer> getRepMentionsSentences() {
		return repMentionsSentences;
	}

	public void setRepMentionsSentences(java.util.Map<String, Integer> repMentionsSentences) {
		this.repMentionsSentences = repMentionsSentences;
	}

}
