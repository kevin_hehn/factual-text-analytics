package de.uni.mannheim.fta.utils.compressions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.spark.input.PortableDataStream;

public class TarGzReader {
    private TarArchiveInputStream tarInput; 
    
    /**
     * Constructor for TarGzReader where bytes are converted to tar archive input stream
     * @param is: a portable data stream from Spark (the content from SparkContext.binaryFiles() )
     * @throws IOException 
     */
    public TarGzReader(PortableDataStream is) throws IOException{
        this.tarInput = new TarArchiveInputStream(new GZIPInputStream(is.open()));
    }
    
    /**
     * Get the next entry from the archive
     * @return Tar archive entry
     */
    public TarArchiveEntry getNextEntry(){
        TarArchiveEntry tarArchiveEntry = null;
        try {
            tarArchiveEntry = this.tarInput.getNextTarEntry();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return tarArchiveEntry;
    }
    
    /**
     * Get the content of the current archive entry as a string
     * @param entry: the currently examined file in the archive
     * @return the current entry's content as a string
     * @throws IOException
     */
    public String getEntryContent(TarArchiveEntry entry) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(this.tarInput));
        String line;
        StringBuilder sb = new StringBuilder((int) entry.getSize());
        while ((line = br.readLine()) != null) {
            // TODO: check if you should use sb.append(line + "\n") 
            sb.append(line);
        }
        return sb.toString();
    }
    
    /**
     * Get the content of the current archive entry as a string
     * @param entry: the currently examined file in the archive
     * @return the current entry's content as a string
     * @throws IOException
     */
    public String getEntryContentNew(TarArchiveEntry entry) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(this.tarInput));
        String line;
        StringBuilder sb = new StringBuilder((int) entry.getSize());
        int len;
	    char[] buffer = new char[1000000];
        while ((len = br.read(buffer))  > 0) {
            // TODO: check if you should use sb.append(line + "\n") 
            sb.append(String.valueOf(buffer,0,len));
            buffer = new char[1000000];
        }
        return sb.toString();
    }
    
}
