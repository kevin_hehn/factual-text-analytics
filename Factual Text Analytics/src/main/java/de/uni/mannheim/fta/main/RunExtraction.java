package de.uni.mannheim.fta.main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapreduce.AvroKeyOutputFormat;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.input.PortableDataStream;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import avroschema.GigaWordsArticle;
import avroschema.NYTArticle;
import avroschema.Sentence;
import avroschema.Token;
import avroschema.WikiArticle;
import de.uni.mannheim.fta.processing.ProcessXML;
import de.uni.mannheim.fta.processing.WikipediaArticle;
import de.uni.mannheim.fta.utils.CoreNLPUtils;
import de.uni.mannheim.fta.utils.avro.AvroUtils;
import de.uni.mannheim.fta.utils.compressions.GzReader;
import de.uni.mannheim.fta.utils.compressions.TarGzReader;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import scala.Tuple1;
import scala.Tuple2;
import scala.collection.Seq;

/**
 * This class is used to do the preprocessing needed for MinIE to run. It works
 * with three different corporas. 
 * 1) Wikipedia, represented as a dump XML 
 * 2) Gigaword 
 * 3) New York Times corpus
 * 
 * @author Kevin
 *
 */
public class RunExtraction {

	// Variable declaration
	public static final StanfordCoreNLP pipeline = CoreNLPUtils.StanfordDepNNParser();
	public static final StanfordCoreNLP pipeline2 = CoreNLPUtils.StanfordSentenceParser();
	private static JavaSparkContext context;
	private static final Logger logger = LogManager.getLogger("Extraction Logger");

	/**
	 * The main method is used as the entry point. From this single method you can launch different steps, depending on what you
	 * want to process.
	 * You can hand over four different paramenter in the args array.
	 * 1) Refers to the corpus: "nyt" -> New York Times; "gigaword" -> GigaWords; "wikipedia" -> Wikipedia
	 * 2) Refers to where the input files are located. PLEASE NOTE: GigaWord & NYT can be run directly from the files. 
	 * 	  For Wikipedia you need to preprocess the dump using Wikiforia first!
	 * 	  When processing the whole nyt dataset, make sure you have topLevelFolder+"\\*\\*.tgz, depending on how deep the nesting is.
	 *    Same goes for gigaword: topLevelFolder+"\\*\\*\\data\\*\\*.gz
	 * 3) Refers to the output directory. ATTENTION: If the folder already exists, it will be deleted before the processing starts.
	 * 4) If set to "true", Spark will run locally. If not the SparkConf will be created using some jars. 
	 * 	  => In case the programm should run on a cluster, please adapt the jars variable (line 100) accordingly.
	 * @param args
	 */
	public static void main(String[] args) {
		//Some example parameter configurations.
		// nyt C:\\Users\\Kevin\\Desktop\\NYT\\02 C:\\Users\\Kevin\\Desktop\\OUTPUT\\ExtractionOutput_NYT true
		// gigaword E:\Factual Text\AIDA_entity_repository_2014-01-02v10.sql\gigaword\gigaword\gigaword_eng_5_d2\data\cna_eng C:\\Users\\Kevin\\Desktop\\OUTPUT\\ExtractionOutput_GIGA_FULL true
		// wikipedia C:\\Users\\Kevin\\Desktop\\OUTPUT\\Wiki1000Input C:\\Users\\Kevin\\Desktop\\OUTPUT\\ExtractionOutput_WIKI true
		String corpus = args[0];
		String inputdir = args[1];
		String outputdir = args[2];
		String useLocalMode = args[3];
		Seq<String> jars = null;

		corpus = corpus.toLowerCase();
		useLocalMode = useLocalMode.toLowerCase();

		//Delete the output directory if it is existing.
		try {
			FileUtils.deleteDirectory(new File(outputdir));
		} catch (IOException e) {
			logger.debug("Something went wrong with the deletion of existing folders.");
		}

		//Depending on the input params, set the spark context to either local or cluster mode.
		SparkConf conf;
		if (useLocalMode.equals("true"))
			conf = new SparkConf().setAppName("Extraction").setMaster("local[3]");// .setJars(jars);
		else
			conf = new SparkConf().setAppName("Extraction").setJars(jars);
		context = new JavaSparkContext(conf);

		//Depending on the input params, launch the respective spark scripts.
		if (corpus.equals("nyt"))
			runNYTExtraction(inputdir, outputdir);
		if (corpus.equals("wikipedia"))
			runWikipediaExtraction(inputdir, outputdir);
		if (corpus.equals("gigaword"))
			runGigaWordExtraction(inputdir, outputdir);

		context.close();
	}
	
	/**
	 * Spark script for the processing of NYT
	 * @param inputDirectory
	 * @param outputDirectory
	 */
	public static void runNYTExtraction(String inputDirectory, String outputDirectory) {
		logger.debug("Start NYT Extraction");
		// Read binary files from the folder. Each file is read as a single
		// record and returned in a key-value pair,
		// where the key is the path of each file, the value is the content of
		// each file.
		JavaPairRDD<String, PortableDataStream> compressedFilesRDD = context.binaryFiles(inputDirectory);

		
		// Decompress the tgz's and get the content of the files as tuples where
		// 'key' is the file name and 'value'
		// the content of the file as a string
		JavaRDD<Tuple2<String, String>> fileContentRDD = compressedFilesRDD.flatMap(fileNameContent -> {
			PortableDataStream content = fileNameContent._2();
			logger.debug(content);
			logger.debug("Start Read File: " + fileNameContent._1());
			List<Tuple2<String, String>> resultTuples = new ArrayList<Tuple2<String, String>>();

			// Iterate through a 'tgz' archive. Each entry is an xml file
			TarGzReader tgzReader = new TarGzReader(content);
			TarArchiveEntry currentEntry = tgzReader.getNextEntry();

			while (currentEntry != null) {
				// Skip the directories
				if (currentEntry.isDirectory()) {
					currentEntry = tgzReader.getNextEntry();
					continue;
				}
				// The file's content and file name
				String fileContent = tgzReader.getEntryContentNew(currentEntry);
				String fileName = currentEntry.getName();
				Tuple2<String, String> t = new Tuple2<String, String>(fileName, fileContent);
				logger.debug("End Read File: " + fileName);
				resultTuples.add(t);
				currentEntry = tgzReader.getNextEntry();
			}
			return resultTuples;
		});

		// Process the articles
		JavaRDD<NYTArticle> articlesRDD = fileContentRDD.map(tuples -> {
			NYTArticle a = new NYTArticle();
			a.setFilename(null);

			// Get the article from the .xml file
			ProcessXML processing = new ProcessXML();
			processing.processNYTXmlFromContent(tuples._2(), pipeline, pipeline2);

			// If there is no content - don't add the article to the list
			if (processing.getContent() != null) {
				a = processing.getArticle();
				a.setFilename(a.getPublicationYear() + "/" + tuples._1());
			}
			logger.debug("Processed Article: " + a.getFilename());
			return a;
		});

		// Filter the articles from 'articleRDD'. Eliminate the ones without
		// content or throwing errors
		JavaRDD<NYTArticle> filteredArticlesRDD = articlesRDD.filter(a -> {
			return !(a.getFilename() == null);
		});

		// Serializing to AVRO
		JavaPairRDD<AvroKey<NYTArticle>, NullWritable> javaPairRDD = filteredArticlesRDD.mapToPair(r -> {
			logger.debug("Serializing to Avro");
			return new Tuple2<AvroKey<NYTArticle>, NullWritable>(new AvroKey<NYTArticle>(r), NullWritable.get());
		});
		Job job = AvroUtils.getJobOutputKeyAvroSchema(NYTArticle.getClassSchema());
		javaPairRDD.saveAsNewAPIHadoopFile(outputDirectory, AvroKey.class, NullWritable.class,
				AvroKeyOutputFormat.class, job.getConfiguration());

	}
	/**
	 * Spark script for processing of wikipedia.
	 * Assumes that the data is preprocessed using wikiforia.
	 * @param inputDirectory
	 * @param outputDirectory
	 */
	public static void runWikipediaExtraction(String inputDirectory, String outputDirectory) {
		logger.debug("Start Wikipedia Extraction");

		JavaRDD<String> data = context.textFile(inputDirectory);
		//Each line in the preprocessed dataset refers to an article, therefore process line by line
		JavaPairRDD<AvroKey<WikiArticle>, NullWritable> javaPairRDD = data.mapToPair(line -> {
			//Use the DOM parser to process each article
			String xml = line;
			DOMParser parser = new DOMParser();
			WikipediaArticle temp = new WikipediaArticle();
			try {
				parser.parse(new InputSource(new java.io.StringReader(xml)));
				xml = null;
				Document doc = parser.getDocument();
				String text = doc.getDocumentElement().getTextContent();
				String id = doc.getDocumentElement().getAttribute("id");
				String title = doc.getDocumentElement().getAttribute("title");
				String revision = doc.getDocumentElement().getAttribute("revision");
				String type = doc.getDocumentElement().getAttribute("type");
				String ns_id = doc.getDocumentElement().getAttribute("ns-id");
				String ns_type = doc.getDocumentElement().getAttribute("ns-name");
				doc = null;
				parser = null;
				temp.setRevision_text(text);
				temp.setId(id);
				temp.setRevision_id(revision);
				temp.setRevision_format(type);
				temp.setNs_id(ns_id);
				temp.setNs(ns_type);
				temp.setTitle(title);
				logger.debug("Read Article with Title: " + title);
			} catch (Exception e) {
				logger.debug("Exception occuring, while parsing the XML");
				logger.debug(e.getMessage());
			}
			// If title or text is null, return null which is filtered out
			// later.
			// Those articles are probably not correct.
			if (temp.getRevision_text() == null || temp.getTitle() == null) {
				logger.debug("Return null");
				return null;
			}
			ProcessXML process = new ProcessXML();
			process.processWikipediaXmlFromContent(temp, pipeline, pipeline2);
			temp = null;
			WikiArticle article = process.getWikiArticle();
			process = null;
			logger.debug("Processed Article with Title: " + article.getTitle());
			int sent = 0;
			int tok = 0;
			for(Sentence s: article.getSentences()) {
				logger.debug(sent);
				for(Token t: s.getTokens()) {
					logger.debug(sent+"_"+tok+"_"+t.getWord());
					tok++;
				}
				logger.debug("\n");
				sent++;
				tok = 0;
			}
			return new Tuple2<AvroKey<WikiArticle>, NullWritable>(new AvroKey<WikiArticle>(article),
					NullWritable.get());
		});
		
		//Filter incorrect articles 
		JavaPairRDD<AvroKey<WikiArticle>, NullWritable> javaPairRDD_filtered = javaPairRDD
				.filter(new Function<Tuple2<AvroKey<WikiArticle>, NullWritable>, Boolean>() {
					private static final long serialVersionUID = -6774546686746962734L;

					@Override
					public Boolean call(Tuple2<AvroKey<WikiArticle>, NullWritable> v1) throws Exception {
						if (v1 == null) {
							logger.debug("Filtered out 1 entry");
						}
						return v1 != null;
					}
				});

		Job job = AvroUtils.getJobOutputKeyAvroSchema(WikiArticle.getClassSchema());

		javaPairRDD_filtered.saveAsNewAPIHadoopFile(outputDirectory, AvroKey.class, NullWritable.class,
				AvroKeyOutputFormat.class, job.getConfiguration());

	}
	/**
	 * Spark script for processing of gigawords.
	 * @param inputDirectory
	 * @param outputDirectory
	 */
	public static void runGigaWordExtraction(String inputDirectory, String outputDirectory) {
		logger.debug("Start GigaWord Extraction...");
		
		JavaPairRDD<String, PortableDataStream> compressedFilesRDD = context.binaryFiles(inputDirectory);
		
		
		// The content of each file is read using a gzip reader and is then
		// stored in a tuple list.
		JavaRDD<Tuple2<String, String>> fileContentRDD = compressedFilesRDD.flatMap(fileNameContent -> {
			PortableDataStream content = fileNameContent._2();
			
			List<Tuple2<String, String>> resultTuples = new ArrayList<Tuple2<String, String>>();
			GzReader gzReader = new GzReader();
			logger.debug("Decompress: "+fileNameContent._1());
			String fileContent = gzReader.unZipFileNew(content);
			String fileName = fileNameContent._1();
			Tuple2<String, String> t = new Tuple2<String, String>(fileName, fileContent);
			resultTuples.add(t);
			logger.debug("Read file: " + fileNameContent._1());
			return resultTuples;
		});

		// One Gigaword file can contain multiple articles. Therefore we need to flatten the articles.
		JavaRDD<Tuple1<GigaWordsArticle>> articlesRDD = fileContentRDD.flatMap(tuples -> {
			logger.debug("articlesRDD - Start: " + tuples._1());
			ArrayList<GigaWordsArticle> a = new ArrayList<GigaWordsArticle>();
			List<Tuple1<GigaWordsArticle>> resultTuples = new ArrayList<Tuple1<GigaWordsArticle>>();
			
			ProcessXML processing = new ProcessXML();
			processing.processGigaWordXmlFromContent(tuples._2(), pipeline, pipeline2);
			a = processing.getGigaWordArticles();
			
			logger.debug("articlesRDD - There are : " + a.size() + " articles in " + tuples._1());
			//Iterate over all gigaword articles contained in the file and flat the list.
			for (int i = 0; i < a.size(); i++) {
				resultTuples.add(new Tuple1<GigaWordsArticle>(a.get(i)));
			}

			return resultTuples;
		});

		// Serializing to AVRO
		JavaPairRDD<AvroKey<GigaWordsArticle>, NullWritable> javaPairRDD = articlesRDD.mapToPair(r -> {
			logger.debug("javaPairRDD - Serialize to AVRO");
			return new Tuple2<AvroKey<GigaWordsArticle>, NullWritable>(new AvroKey<GigaWordsArticle>(r._1()),
					NullWritable.get());
		});
		Job job = AvroUtils.getJobOutputKeyAvroSchema(GigaWordsArticle.getClassSchema());
		javaPairRDD.saveAsNewAPIHadoopFile(outputDirectory, AvroKey.class, NullWritable.class,
				AvroKeyOutputFormat.class, job.getConfiguration());

	}

}
