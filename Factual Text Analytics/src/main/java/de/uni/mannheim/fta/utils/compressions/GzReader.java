package de.uni.mannheim.fta.utils.compressions;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.spark.input.PortableDataStream;

public class GzReader {
	private static final Logger logger = LogManager.getLogger("Process XML Details Logger");
//	private static final String INPUT_GZIP_FILE = "C:/Users/Kevin/Desktop/gigaword/gigaword/gigaword_eng_5_d2/data/cna_eng/cna_eng_199709.gz";
//	public static void main(String[] args) {
//	    GZIPInputStream in = null;
//		try {
//			in = new GZIPInputStream(new FileInputStream(INPUT_GZIP_FILE));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	    Reader decoder = new InputStreamReader(in);
//		BufferedReader br = new BufferedReader(decoder);
//	    String line;
//	     try {
//			while ((line = br.readLine()) != null) {
//			     System.out.println(line);
//			 }
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	   }
	@SuppressWarnings("resource")
	public String unZipFile(PortableDataStream filePath) throws FileNotFoundException, IOException{
		GZIPInputStream in = null;
		in = new GZIPInputStream(filePath.open(),6553600);
		Reader decoder = new InputStreamReader(in);
		BufferedReader br = new BufferedReader(decoder,6553600);
	    String line = "";
	    String output = "";
	    while ((line = br.readLine()) != null) {
		     output += line;
		     output += System.getProperty("line.separator");
		 }
	    return output;
	}
	
	@SuppressWarnings("resource")
	public String unZipFileNew(PortableDataStream filePath) throws FileNotFoundException, IOException{
		GZIPInputStream in = null;
		in = new GZIPInputStream(filePath.open(),6553600);
		Reader decoder = new InputStreamReader(in);
		BufferedReader br = new BufferedReader(decoder,6553600);
	    String output = "";
	    int len;
	    char[] buffer = new char[1000000];
	    while ((len = br.read(buffer))  > 0) {
		     output += String.valueOf(buffer,0,len);
		     buffer = new char[1000000];
		 }
	    br.close();
	    decoder.close();
	    in.close();
	    return output;
	    
	}

	}
