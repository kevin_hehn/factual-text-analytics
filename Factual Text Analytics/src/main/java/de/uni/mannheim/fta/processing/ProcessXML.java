package de.uni.mannheim.fta.processing;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import avroschema.GigaWordsArticle;
import avroschema.NYTArticle;
import avroschema.Sentence;
import avroschema.Token;
import avroschema.WikiArticle;
import de.uni.mannheim.fta.utils.Utils;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

/**
 * This class is used to transform raw data files into an Article object which
 * can be serialized to disk using AVRO. This is currently supported for
 * Wikipedia, New York Times and Gigawords
 * 
 * @author Kiril Gashteovski,Kevin
 *
 */
public class ProcessXML {
	// Variable declaration
	private static final Logger logger = LogManager.getLogger("Process XML Details Logger");
	private File xmlfile;
	java.util.Map<String, Integer> paragraphRepMentionsSentences;
	private StanfordCoreNLP pipelineDepParser;
	private StanfordCoreNLP pipelineSentenceSplitter;
	private NYTArticle article;
	private GigaWordsArticle gigaArticle;
	private ArrayList<GigaWordsArticle> gigaArticles;
	private WikiArticle wikiArticle;
	private TextDocument content;
	List<Sentence> paragraphSentences;
	// If true, the script will process the files which have no content but have
	// other attributes (date, author, ...).
	// If false (by default), processXMLArticle will store for 'article' and
	// 'content' a null value
	private boolean keepEmptyContentFiles = false;

	/**
	 * Constructor for NYT article
	 * 
	 * @param a
	 */
	public ProcessXML(NYTArticle a) {
		this.article = a;
	}

	/**
	 * Default constructor
	 */
	public ProcessXML() {
	}

	/**
	 * Used for NYT Documents
	 * 
	 * @param xmlContent
	 * @param pipeline
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public void processNYTXmlFromContent(String xmlContent, StanfordCoreNLP pipeline, StanfordCoreNLP pipeline2)
			throws SAXException, IOException, ParserConfigurationException {

		// Attributes initialization
		this.pipelineDepParser = pipeline;
		this.pipelineSentenceSplitter = pipeline2;
		this.article = new NYTArticle();

		// NY Times parsing
		NYTCorpusDocumentParser parser = new NYTCorpusDocumentParser();
		NYTCorpusDocument nytDocument = this.getNYTDocumentFromXmlContent(xmlContent, parser);
		this.setArticleAttributes(nytDocument);
	}

	/**
	 * Get NYTCorpusDocument given the content of the xml file
	 * 
	 * @param xmlData:
	 *            the xml content
	 * @param parser:
	 *            NY Times parser
	 * @return NYTCorpusDocument object, parsed
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public NYTCorpusDocument getNYTDocumentFromXmlContent(String xmlData, NYTCorpusDocumentParser parser)
			throws SAXException, IOException, ParserConfigurationException {

		xmlData = xmlData.replace(
				"<!DOCTYPE nitf " + "SYSTEM \"http://www.nitf.org/" + "IPTC/NITF/3.3/specification/dtd/nitf-3-3.dtd\">",
				"");
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(false);
		InputStream is = new ByteArrayInputStream(xmlData.getBytes("UTF8"));
		Document doc = factory.newDocumentBuilder().parse(is);

		NYTCorpusDocument ldcDocument = new NYTCorpusDocument();

		NodeList children = doc.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node child = children.item(i);
			String name = child.getNodeName();
			if (name.equals("nitf")) {
				parser.handleNITFNode(child, ldcDocument);
				// parser.handleNITFNode(child, ldcDocument);
			}
		}

		return ldcDocument;
	}

	/**
	 * Used for GigaWord XML Files
	 * 
	 * @param xmlContent
	 * @param pipeline
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public void processGigaWordXmlFromContent(String xmlContent, StanfordCoreNLP pipeline, StanfordCoreNLP pipeline2)
			throws SAXException, IOException, ParserConfigurationException {

		// Attributes initialization
		this.pipelineDepParser = pipeline;
		this.pipelineSentenceSplitter = pipeline2;
		this.gigaArticles = new ArrayList<GigaWordsArticle>();
		this.gigaArticle = new GigaWordsArticle();

		// Create an object out of the XML file
		GigaWordsAdapter gigaWordDocument = new GigaWordsAdapter();
		gigaWordDocument.startAdapter(xmlContent, 1);
		ArrayList<RawGigaWordArticle> list = gigaWordDocument.getArticleList();

		for (RawGigaWordArticle gigaDocument : list) {
			
			String documentID = gigaDocument.getDocumentID().toLowerCase();
			//nyt_eng_200704
			String corpus = documentID.substring(0,3);
			//Just filter out NYT Articles
			if(corpus.equals("nyt")){
				logger.debug("Article from NYT Corpus"+documentID);
				String year = documentID.substring(8, 12);
				String month = documentID.substring(12,14);
				int intYear = Integer.parseInt(year);
				int intMonth = Integer.parseInt(month);
				logger.debug("Article from Year "+year+" "+month);
				//Filter out Articles already present in NYT Corpus
				if(intYear >=2007 && intMonth > 6){
					logger.debug("Filtered in: "+documentID);
					this.setArticleAttributes(gigaDocument);
					gigaArticles.add(this.getGigaArticle());
					this.gigaArticle = new GigaWordsArticle();
				}
				else{
					logger.debug("Filtered out: "+documentID);
				}
				
				
			}
			else{
				this.setArticleAttributes(gigaDocument);
				gigaArticles.add(this.getGigaArticle());
				this.gigaArticle = new GigaWordsArticle();
			}
		}
	}

	public void processWikipediaXmlFromContent(WikipediaArticle wikiarticle, StanfordCoreNLP pipeline,
			StanfordCoreNLP pipeline2) throws SAXException, IOException, ParserConfigurationException {

		// Attributes initialization
		this.pipelineDepParser = pipeline;
		this.pipelineSentenceSplitter = pipeline2;
		// this.gigaArticles = new ArrayList<Article>();
		this.wikiArticle = new WikiArticle();

		this.setArticleAttributes(wikiarticle);
		this.pipelineDepParser = null;
		this.pipelineSentenceSplitter = null;
	}

	// Set the article attributes from a NYTCorpusDocument object
	public void setArticleAttributes(NYTCorpusDocument nytDocument) {
		// Check if the document has no content
		if ((nytDocument.getBody() == null) && (keepEmptyContentFiles == false)) {
			this.article = null;
			this.content = null;
			return;
		}

		// Content and list of sentences
//		this.content = new TextDocument(nytDocument.getBody(), this.pipelineDepParser);
//		this.article.setSentences(this.content.getSentences(0));

		//Annotated the text, to count the number of sentences. If there are more than 100 sentences, the processing
		//is split up.
		Annotation documentSentences = new Annotation(nytDocument.getBody());
		pipelineSentenceSplitter.annotate(documentSentences);
		List<CoreMap> corSentences = documentSentences.get(SentencesAnnotation.class);
		Map<String, Boolean> tempMap = new HashMap<String, Boolean>();
		
		for (int i = 0; i < corSentences.size(); i++) {
			CoreMap currentSentence = corSentences.get(i);
			edu.stanford.nlp.simple.Sentence sentence = new edu.stanford.nlp.simple.Sentence(currentSentence);
			//If the sign for a new paragraph occurs in a sentence, store this information.
			if (sentence.text().contains("[##|##]")) {
				tempMap.put(Integer.toString(i), true);
			}

		}
		//If the size of a article is > 100 split the processing up.
		if (corSentences.size() > 100) {
			List<Sentence> sentences = new ArrayList<Sentence>();
			java.util.Map<String, String> repMentions = new java.util.HashMap<String, String>();
			paragraphRepMentionsSentences = new java.util.HashMap<String, Integer>();
			java.util.Map<String, Integer> repMentionsSentences = new java.util.HashMap<String, Integer>();
			java.util.HashSet<java.lang.String> locations = new java.util.HashSet<java.lang.String>();
			java.util.HashSet<java.lang.String> people = new java.util.HashSet<java.lang.String>();
			java.util.HashSet<java.lang.String> organizations = new java.util.HashSet<java.lang.String>();
			
			String allParagraphs = nytDocument.getBody();
			// Split the text into its paragraphs, these are defined by [##|##]
			String[] paragraphArray = allParagraphs.split("\\[##\\|##\\]");
			logger.debug("Article " + nytDocument.getHeadline() + " split in " + paragraphArray.length + " Paragraphs");
			
			int paragraphOffset = 0;
			for (String paragraph : paragraphArray) {
				Annotation document = new Annotation(paragraph);
				pipelineSentenceSplitter.annotate(document);
				TextDocument paragraphDocument;
				List<CoreMap> coreMapSentences = document.get(SentencesAnnotation.class);
				int processingThreshold = 100;
				int offSet = 0;
				int counter = 0;
				String changedParagraph = "";
				for (CoreMap currentSentence : coreMapSentences) {
					edu.stanford.nlp.simple.Sentence countTokensSentence = new edu.stanford.nlp.simple.Sentence(
							currentSentence);
					// IF SENTENCE HAS MORE THAN 100 TOKENS, DO NOT PROCESS IT!
					if (countTokensSentence.length() > 100) {
						continue;
					}
					changedParagraph += " " + currentSentence.toString();
					counter++;
					if (counter >= processingThreshold) {
						paragraphDocument = new TextDocument(changedParagraph, pipelineDepParser);
						paragraphSentences = paragraphDocument
								.getSentences((offSet * processingThreshold) + paragraphOffset);
						locations.addAll(paragraphDocument.getLocations());
						people.addAll(paragraphDocument.getPeople());
						organizations.addAll(paragraphDocument.getOrganizations());
						paragraphRepMentionsSentences = paragraphDocument.getRepMentionsSentences();
						repMentions = mergeMaps(repMentions, paragraphDocument.getRepMentions());
						repMentionsSentences = mergeMapsInteger(repMentionsSentences, paragraphRepMentionsSentences);
						for (Sentence s : paragraphSentences) {
							sentences.add(s);
						}
						changedParagraph = "";
						counter = 0;
						offSet++;
					}
				}
				paragraphDocument = new TextDocument(changedParagraph, pipelineDepParser);
				paragraphSentences = paragraphDocument.getSentences(((offSet * processingThreshold) + paragraphOffset));
				locations.addAll(paragraphDocument.getLocations());
				people.addAll(paragraphDocument.getPeople());
				organizations.addAll(paragraphDocument.getOrganizations());
				paragraphRepMentionsSentences = paragraphDocument.getRepMentionsSentences();
				repMentions = mergeMaps(repMentions, paragraphDocument.getRepMentions());
				repMentionsSentences = mergeMapsInteger(repMentionsSentences, paragraphRepMentionsSentences);
				for (Sentence s : paragraphSentences) {
					sentences.add(s);
					paragraphOffset = sentences.size();
				}
			}

			/*
			 * END NEW
			 */
			this.article.setSentences(sentences);
			this.article.setRepresentativeMention(repMentions);
			this.article.setLocations(new ArrayList<String>(locations));
			this.article.setOrganizations(new ArrayList<String>(organizations));
			this.article.setPeople(new ArrayList<String>(people));
			this.article.setRepresentativeMentionSentenceId(repMentionsSentences);
		} else {
			//Make sure there are no sentences containing more than 100 Tokens!!
			Annotation document = new Annotation(nytDocument.getBody());
			pipelineSentenceSplitter.annotate(document);
			List<CoreMap> coreMapSentences = document.get(SentencesAnnotation.class);
			String changedText = "";
			for (CoreMap currentSentence : coreMapSentences) {
				edu.stanford.nlp.simple.Sentence countTokensSentence = new edu.stanford.nlp.simple.Sentence(
						currentSentence);
				if (countTokensSentence.length() > 100) {
					continue;
				}
				changedText += " " + currentSentence.toString();
			}
			
			this.content = new TextDocument(changedText, this.pipelineDepParser);
			this.article.setSentences(this.content.getSentences(0));
			this.article.setRepresentativeMention(this.content.getRepMentions());
			this.article.setLocations(new ArrayList<String>(this.content.getLocations()));
			this.article.setOrganizations(new ArrayList<String>(this.content.getOrganizations()));
			this.article.setPeople(new ArrayList<String>(this.content.getPeople()));
			this.article.setRepresentativeMentionSentenceId(this.content.getRepMentionsSentences());
		}

		// Create article object from the AVRO schema
		this.article.setAbstract$(nytDocument.getArticleAbstract() == null ? "" : nytDocument.getArticleAbstract());
		this.article.setFilename("");
		// Merge People Data with what was found by CoreNLP
		List<String> tempPeople = this.article.getPeople();
		tempPeople.addAll(nytDocument.getPeople());
		HashSet<String> tempPeopleSet = new HashSet<String>(tempPeople);
		this.article.setPeople(new ArrayList<String>(tempPeopleSet));

		this.article.setParagraphs(tempMap);
		this.article.setPublicationMonth(Utils.checkNull(nytDocument.getPublicationMonth()));
		this.article.setPrintPageNumber(Utils.checkNull(nytDocument.getPage()));
		this.article.setTitles(nytDocument.getTitles());
		this.article.setPublicationYear(Utils.checkNull(nytDocument.getPublicationYear()));
		this.article.setPublicationDayOfMonth(Utils.checkNull(nytDocument.getPublicationDayOfMonth()));
		this.article.setAlternateUrl(Utils.checkNull(nytDocument.getAlternateURL()));
		this.article.setTitle(Utils.checkNull(nytDocument.getHeadline()));
		this.article.setFeaturePage(nytDocument.getFeaturePage());
		this.article.setCorrectionDate(Utils.checkNull(nytDocument.getCorrectionDate()));
		this.article.setDsk(Utils.checkNull(nytDocument.getNewsDesk()));
		this.article.setContent(Utils.checkNull(nytDocument.getBody()));
		this.article.setPublicationDayOfWeek(Utils.checkNull(nytDocument.getDayOfWeek()));
		this.article.setOnlineSections(Utils.checkNull(nytDocument.getOnlineSection()));
		this.article.setColumnName(Utils.checkNull(nytDocument.getColumnName()));
		this.article.setBanner(Utils.checkNull(nytDocument.getBanner()));
		this.article.setSlug(Utils.checkNull(nytDocument.getSlug()));
		this.article.setPrintColumn(Utils.checkNull(nytDocument.getColumnNumber()));
		this.article.setLeadParagraph(Utils.checkNull(nytDocument.getLeadParagraph()));
		this.article.setUrl(Utils.checkNull(nytDocument.getUrl()));
		this.article.setPrintSection(Utils.checkNull(nytDocument.getSection()));
		this.article.setOnlineHeadline(Utils.checkNull(nytDocument.getOnlineHeadline()));
		this.article.setOnlineTitles(nytDocument.getOnlineTitles());

		// Merge Locations with what was found by CoreNLP
		List<String> tempLocations = this.article.getLocations();
		tempLocations.addAll(nytDocument.getLocations());
		HashSet<String> tempLocationsSet = new HashSet<String>(tempLocations);
		this.article.setLocations(new ArrayList<String>(tempLocationsSet));

		this.article.setNames(nytDocument.getNames());
		this.article.setDescriptors(nytDocument.getDescriptors());
		this.article.setGeneralDescriptors(nytDocument.getGeneralOnlineDescriptors());
		this.article.setAuthorInfo(Utils.checkNull(nytDocument.getAuthorBiography()));
		this.article.setSeriesName(Utils.checkNull(nytDocument.getSeriesName()));
		this.article.setNormalizedByline(Utils.checkNull(nytDocument.getNormalizedByline()));
		this.article.setOnlinePeople(nytDocument.getOnlinePeople());
		this.article.setDateline(Utils.checkNull(nytDocument.getDateline()));

		// Merge Organizations with what was found by CoreNLP
		List<String> tempOrganizations = this.article.getOrganizations();
		tempOrganizations.addAll(nytDocument.getOrganizations());
		HashSet<String> tempOrganizationsSet = new HashSet<String>(tempOrganizations);
		this.article.setOrganizations(new ArrayList<String>(tempOrganizationsSet));

		this.article.setOnlineOrganizations(nytDocument.getOnlineOrganizations());
		this.article.setTaxonomicClassifiers(nytDocument.getTaxonomicClassifiers());
		this.article.setTypesOfMaterial(nytDocument.getTypesOfMaterial());
		this.article.setWordCount(nytDocument.getWordCount());

	}

	public void setArticleAttributes(RawGigaWordArticle gigaDocument) {
		// Check if the document has no content
		if ((gigaDocument.getFullText() == null) && (keepEmptyContentFiles == false)) {
			this.gigaArticle = null;
			this.content = null;
			return;
		}
		// System.out.println(gigaDocument.getFullText());
		ArrayList<String> dummy = new ArrayList<String>();
		dummy.add("");
		/*
		 * NEW
		 */
		// Content and list of sentences
		// this.content = new TextDocument(gigaDocument.getFullText(),
		// this.pipelineDepParser);
		Annotation documentSentences = new Annotation(gigaDocument.getFullText());
		pipelineSentenceSplitter.annotate(documentSentences);
		List<CoreMap> corSentences = documentSentences.get(SentencesAnnotation.class);
		Map<String, Boolean> tempMap = new HashMap<String, Boolean>();
		for (int i = 0; i < corSentences.size(); i++) {
			CoreMap currentSentence = corSentences.get(i);
			edu.stanford.nlp.simple.Sentence sentence = new edu.stanford.nlp.simple.Sentence(currentSentence);
			if (sentence.text().contains("[##|##]")) {
				tempMap.put(Integer.toString(i), true);
			}

		}
		this.gigaArticle.setParagraphs(tempMap);
		if (corSentences.size() > 100) {
			String allParagraphs = gigaDocument.getFullText();
			// Split the text into its paragraphs, these are defined by [##|##]
			String[] paragraphArray = allParagraphs.split("\\[##\\|##\\]");
			logger.debug(
					"Article " + gigaDocument.getHeadline() + " split in " + paragraphArray.length + " Paragraphs");
			List<Sentence> sentences = new ArrayList<Sentence>();
			java.util.Map<String, String> repMentions = new java.util.HashMap<String, String>();
			paragraphRepMentionsSentences = new java.util.HashMap<String, Integer>();
			java.util.Map<String, Integer> repMentionsSentences = new java.util.HashMap<String, Integer>();
			java.util.HashSet<java.lang.String> locations = new java.util.HashSet<java.lang.String>();
			java.util.HashSet<java.lang.String> people = new java.util.HashSet<java.lang.String>();
			java.util.HashSet<java.lang.String> organizations = new java.util.HashSet<java.lang.String>();
			int paragraphOffset = 0;
			for (String paragraph : paragraphArray) {
				Annotation document = new Annotation(paragraph);
				pipelineSentenceSplitter.annotate(document);
				TextDocument paragraphDocument;
				List<CoreMap> coreMapSentences = document.get(SentencesAnnotation.class);
				int processingThreshold = 100;
				int offSet = 0;
				int counter = 0;
				String changedParagraph = "";
				for (CoreMap currentSentence : coreMapSentences) {
					edu.stanford.nlp.simple.Sentence countTokensSentence = new edu.stanford.nlp.simple.Sentence(
							currentSentence);
					// IF SENTENCE HAS MORE THAN 100 TOKENS, DO NOT PROCESS IT!
					if (countTokensSentence.length() > 100) {
						continue;
					}
					changedParagraph += " " + currentSentence.toString();
					counter++;
					if (counter >= processingThreshold) {
						paragraphDocument = new TextDocument(changedParagraph, pipelineDepParser);
						paragraphSentences = paragraphDocument
								.getSentences((offSet * processingThreshold) + paragraphOffset);
						locations.addAll(paragraphDocument.getLocations());
						people.addAll(paragraphDocument.getPeople());
						organizations.addAll(paragraphDocument.getOrganizations());
						paragraphRepMentionsSentences = paragraphDocument.getRepMentionsSentences();
						repMentions = mergeMaps(repMentions, paragraphDocument.getRepMentions());
						repMentionsSentences = mergeMapsInteger(repMentionsSentences, paragraphRepMentionsSentences);
						for (Sentence s : paragraphSentences) {
							sentences.add(s);
						}
						changedParagraph = "";
						counter = 0;
						offSet++;
					}
				}
				paragraphDocument = new TextDocument(changedParagraph, pipelineDepParser);
				paragraphSentences = paragraphDocument.getSentences(((offSet * processingThreshold) + paragraphOffset));
				locations.addAll(paragraphDocument.getLocations());
				people.addAll(paragraphDocument.getPeople());
				organizations.addAll(paragraphDocument.getOrganizations());
				paragraphRepMentionsSentences = paragraphDocument.getRepMentionsSentences();
				repMentions = mergeMaps(repMentions, paragraphDocument.getRepMentions());
				repMentionsSentences = mergeMapsInteger(repMentionsSentences, paragraphRepMentionsSentences);
				for (Sentence s : paragraphSentences) {
					sentences.add(s);
					paragraphOffset = sentences.size();
				}
			}
			/*
			 * END NEW
			 */
			this.gigaArticle.setSentences(sentences);
			this.gigaArticle.setDocID(gigaDocument.getDocumentID()==null?"":gigaDocument.getDocumentID());
			this.gigaArticle.setStory(gigaDocument.getDocumentType()==null?"":gigaDocument.getDocumentType());
			this.gigaArticle.setDateline(gigaDocument.getDateline()==null?"":gigaDocument.getDateline());
			this.gigaArticle.setHeadline(gigaDocument.getHeadline()==null?"":gigaDocument.getHeadline());
			this.gigaArticle.setContent(gigaDocument.getFullText().replaceAll("\\[##\\|##\\]", "")==null?"":gigaDocument.getFullText().replaceAll("\\[##\\|##\\]", ""));
			this.gigaArticle.setRepresentativeMention(repMentions);
			this.gigaArticle.setWordCount(0);
			this.gigaArticle.setLocations(new ArrayList<String>(locations));
			this.gigaArticle.setOrganizations(new ArrayList<String>(organizations));
			this.gigaArticle.setPeople(new ArrayList<String>(people));
			this.gigaArticle.setRepresentativeMentionSentenceId(repMentionsSentences);
		} else {
			Annotation document = new Annotation(gigaDocument.getFullText().replaceAll("\\[##\\|##\\]", ""));
			pipelineSentenceSplitter.annotate(document);
			List<CoreMap> coreMapSentences = document.get(SentencesAnnotation.class);
			String changedText = "";
			for (CoreMap currentSentence : coreMapSentences) {
				edu.stanford.nlp.simple.Sentence countTokensSentence = new edu.stanford.nlp.simple.Sentence(
						currentSentence);
				if (countTokensSentence.length() > 100) {
					continue;
				}
				changedText += " " + currentSentence.toString();
			}
			String text = changedText;
			this.content = new TextDocument(text, this.pipelineDepParser);
			this.gigaArticle.setSentences(this.content.getSentences(0));
			this.gigaArticle.setDocID(gigaDocument.getDocumentID()==null?"":gigaDocument.getDocumentID());
			this.gigaArticle.setStory(gigaDocument.getDocumentType()==null?"":gigaDocument.getDocumentType());
			this.gigaArticle.setDateline(gigaDocument.getDateline()==null?"":gigaDocument.getDateline());
			this.gigaArticle.setHeadline(gigaDocument.getHeadline()==null?"":gigaDocument.getHeadline());
			this.gigaArticle.setContent(gigaDocument.getFullText()==null?"":gigaDocument.getFullText());
			this.gigaArticle.setRepresentativeMention(this.content.getRepMentions());
			this.gigaArticle.setWordCount(0);
			this.gigaArticle.setLocations(new ArrayList<String>(this.content.getLocations()));
			this.gigaArticle.setOrganizations(new ArrayList<String>(this.content.getOrganizations()));
			this.gigaArticle.setPeople(new ArrayList<String>(this.content.getPeople()));
			this.gigaArticle.setRepresentativeMentionSentenceId(this.content.getRepMentionsSentences());
			logger.debug("Processed: " + gigaDocument.getHeadline());
		}
	}

	public void setArticleAttributes(WikipediaArticle wikiDocument) {
		// Check if the document has no content
		if ((wikiDocument.getRevision_text() == null) && (keepEmptyContentFiles == false)) {
			this.wikiArticle = null;
			this.content = null;
			return;
		}
		ArrayList<String> dummy = new ArrayList<String>();
		dummy.add("");
		/*
		 * NEW
		 */
		String allParagraphs = wikiDocument.getRevision_text();
		// Split the text into its paragraphs, these are defined by [##|##]
		String[] paragraphArray = allParagraphs.split("\\[##\\|##\\]");
		logger.debug("Article " + wikiDocument.getTitle() + " split in " + paragraphArray.length + " Paragraphs");
		List<Sentence> sentences = new ArrayList<Sentence>();
		java.util.Map<String, String> repMentions = new java.util.HashMap<String, String>();
		paragraphRepMentionsSentences = new java.util.HashMap<String, Integer>();
		java.util.Map<String, Integer> repMentionsSentences = new java.util.HashMap<String, Integer>();
		java.util.HashSet<java.lang.String> locations = new java.util.HashSet<java.lang.String>();
		java.util.HashSet<java.lang.String> people = new java.util.HashSet<java.lang.String>();
		java.util.HashSet<java.lang.String> organizations = new java.util.HashSet<java.lang.String>();
		int paragraphOffset = 0;
		Annotation documentSentences = new Annotation(wikiDocument.getRevision_text());
		pipelineSentenceSplitter.annotate(documentSentences);
		List<CoreMap> corSentences = documentSentences.get(SentencesAnnotation.class);
		Map<String, Boolean> tempMap = new HashMap<String, Boolean>();
		for (int i = 0; i < corSentences.size(); i++) {
			CoreMap currentSentence = corSentences.get(i);
			edu.stanford.nlp.simple.Sentence sentence = new edu.stanford.nlp.simple.Sentence(currentSentence);
			if (sentence.text().contains("[##|##]")) {
				tempMap.put(Integer.toString(i), true);
			}
		}
		this.wikiArticle.setParagraphs(tempMap);
		for (String paragraph : paragraphArray) {
			Annotation document = new Annotation(paragraph);
			pipelineSentenceSplitter.annotate(document);
			TextDocument paragraphDocument;
			List<CoreMap> coreMapSentences = document.get(SentencesAnnotation.class);
			int processingThreshold = 100;
			int offSet = 0;
			int counter = 0;
			String changedParagraph = "";
			for (CoreMap currentSentence : coreMapSentences) {
				edu.stanford.nlp.simple.Sentence countTokensSentence = new edu.stanford.nlp.simple.Sentence(
						currentSentence);
				// IF SENTENCE HAS MORE THAN 100 TOKENS, DO NOT PROCESS IT!
				if (countTokensSentence.length() > 100) {
					continue;
				}
				changedParagraph += " " + currentSentence.toString();
				counter++;
				if (counter >= processingThreshold) {
					paragraphDocument = new TextDocument(changedParagraph, pipelineDepParser);
					paragraphSentences = paragraphDocument
							.getSentences((offSet * processingThreshold) + paragraphOffset);
					locations.addAll(paragraphDocument.getLocations());
					people.addAll(paragraphDocument.getPeople());
					organizations.addAll(paragraphDocument.getOrganizations());
					paragraphRepMentionsSentences = paragraphDocument.getRepMentionsSentences();
					repMentions = mergeMaps(repMentions, paragraphDocument.getRepMentions());
					repMentionsSentences = mergeMapsInteger(repMentionsSentences, paragraphRepMentionsSentences);
					for (Sentence s : paragraphSentences) {
						sentences.add(s);
					}
					changedParagraph = "";
					counter = 0;
					offSet++;
				}
			}
			paragraphDocument = new TextDocument(changedParagraph, pipelineDepParser);
			paragraphSentences = paragraphDocument.getSentences(((offSet * processingThreshold) + paragraphOffset));
			locations.addAll(paragraphDocument.getLocations());
			people.addAll(paragraphDocument.getPeople());
			organizations.addAll(paragraphDocument.getOrganizations());
			paragraphRepMentionsSentences = paragraphDocument.getRepMentionsSentences();
			repMentions = mergeMaps(repMentions, paragraphDocument.getRepMentions());
			repMentionsSentences = mergeMapsInteger(repMentionsSentences, paragraphRepMentionsSentences);
			for (Sentence s : paragraphSentences) {
				sentences.add(s);
				paragraphOffset = sentences.size();
			}
		}
		/*
		 * END NEW
		 */
		this.wikiArticle.setSentences(sentences);
		this.wikiArticle.setId(Integer.parseInt(wikiDocument.getId()));
		this.wikiArticle.setContent(wikiDocument.getRevision_text());
		this.wikiArticle.setNsId(wikiDocument.getNs_id());
		this.wikiArticle.setNsName(wikiDocument.getNs());
		this.wikiArticle.setRevision(wikiDocument.getRevision_id());
		this.wikiArticle.setTitle(wikiDocument.getTitle());
		this.wikiArticle.setRepresentativeMention(repMentions);
		this.wikiArticle.setWordCount(0);
		this.wikiArticle.setLocations(new ArrayList<String>(locations));
		this.wikiArticle.setOrganizations(new ArrayList<String>(organizations));
		this.wikiArticle.setPeople(new ArrayList<String>(people));
		this.wikiArticle.setRepresentativeMentionSentenceId(repMentionsSentences);
	}

	public void serializeArticleToDisk(String filename) throws IOException {
		// Serialize article to disk
		DatumWriter<NYTArticle> articleDatumWriter = new SpecificDatumWriter<NYTArticle>(NYTArticle.class);
		DataFileWriter<NYTArticle> dataFileWriter = new DataFileWriter<NYTArticle>(articleDatumWriter);
		dataFileWriter.create(this.article.getSchema(), new File(filename));
		dataFileWriter.append(this.article);
		dataFileWriter.close();
	}

	public static void serializeArticleToDisk(String filename, NYTArticle article) throws IOException {
		// Serialize article to disk
		DatumWriter<NYTArticle> articleDatumWriter = new SpecificDatumWriter<NYTArticle>(NYTArticle.class);
		DataFileWriter<NYTArticle> dataFileWriter = new DataFileWriter<NYTArticle>(articleDatumWriter);
		dataFileWriter.create(article.getSchema(), new File(filename));
		dataFileWriter.append(article);
		dataFileWriter.close();
	}

	/**
	 * Getters
	 */
	public List<Sentence> getSentences() {
		// If the article has no content, return an empty list of sentences
		if (content != null)
			return this.content.getSentences(0);
		else
			return new ArrayList<Sentence>();
	}

	public NYTArticle getArticle() {
		this.pipelineDepParser = null;
		this.pipelineSentenceSplitter = null;
		return this.article;
	}

	public WikiArticle getWikiArticle() {
		this.pipelineDepParser = null;
		this.pipelineSentenceSplitter = null;
		return this.wikiArticle;
	}

	public GigaWordsArticle getGigaArticle() {
		return this.gigaArticle;
	}

	public ArrayList<GigaWordsArticle> getGigaWordArticles() {
		this.pipelineDepParser = null;
		this.pipelineSentenceSplitter = null;
		return this.gigaArticles;

	}

	public TextDocument getContent() {
		return this.content;
	}

	public boolean getKeepEmptyContentFiles() {
		return this.keepEmptyContentFiles;
	}

	/**
	 * Setters
	 */
	public void setArticle(NYTArticle a) {
		this.article = a;
	}

	public void setKeepEmptyContentFiles(boolean c) {
		this.keepEmptyContentFiles = c;
	}

	public Map<String, String> mergeMaps(Map<String, String> a, Map<String, String> b) {
		Map<String, String> output = new HashMap<String, String>();
		output.putAll(a);
		Random rand = new Random();
		for (String s : b.keySet()) {
			if (output.containsKey(s)) {
				String n = s;
				while (output.containsKey(n)) {
					int temp = rand.nextInt(500000) + 1;
					n = Integer.toString(temp);
				}
				output.put(n, b.get(s));
				for (int i = 0; i < paragraphSentences.size(); i++) {
					Sentence tempSentence = paragraphSentences.get(i);
					List<Token> tempTokenList = tempSentence.getTokens();
					for (int ii = 0; ii < paragraphSentences.get(i).getTokens().size(); ii++) {
						if (paragraphSentences.get(i).getTokens().get(ii).getChainId().equals(s)) {
							Token t = paragraphSentences.get(i).getTokens().get(ii);
							t.setChainId(n);
							tempTokenList.set(ii, t);
							tempSentence.setTokens(tempTokenList);
							paragraphSentences.set(i, tempSentence);
						}

					}
				}
				// Update Sentence ID
				int value = paragraphRepMentionsSentences.remove(s);
				paragraphRepMentionsSentences.put(n, value);
			} else {
				output.put(s, b.get(s));
			}
		}
		return output;
	}

	public Map<String, Integer> mergeMapsInteger(Map<String, Integer> a, Map<String, Integer> b) {
		Map<String, Integer> output = new HashMap<String, Integer>();
		output.putAll(a);
		output.putAll(b);
		return output;
	}

	public void processTextSplit(String fullText, String headline) {
		Annotation documentSentences = new Annotation(fullText);
		pipelineSentenceSplitter.annotate(documentSentences);
		List<CoreMap> corSentences = documentSentences.get(SentencesAnnotation.class);
		Map<String, Boolean> tempMap = new HashMap<String, Boolean>();
		for (int i = 0; i < corSentences.size(); i++) {
			CoreMap currentSentence = corSentences.get(i);
			edu.stanford.nlp.simple.Sentence sentence = new edu.stanford.nlp.simple.Sentence(currentSentence);
			if (sentence.text().contains("[##|##]")) {
				tempMap.put(Integer.toString(i), true);
			}

		}

		if (corSentences.size() > 100) {
			String allParagraphs = fullText;
			// Split the text into its paragraphs, these are defined by [##|##]
			String[] paragraphArray = allParagraphs.split("\\[##\\|##\\]");
			logger.debug("Article " + headline + " split in " + paragraphArray.length + " Paragraphs");
			List<Sentence> sentences = new ArrayList<Sentence>();
			java.util.Map<String, String> repMentions = new java.util.HashMap<String, String>();
			paragraphRepMentionsSentences = new java.util.HashMap<String, Integer>();
			java.util.Map<String, Integer> repMentionsSentences = new java.util.HashMap<String, Integer>();
			java.util.HashSet<java.lang.String> locations = new java.util.HashSet<java.lang.String>();
			java.util.HashSet<java.lang.String> people = new java.util.HashSet<java.lang.String>();
			java.util.HashSet<java.lang.String> organizations = new java.util.HashSet<java.lang.String>();
			int paragraphOffset = 0;
			for (String paragraph : paragraphArray) {
				Annotation document = new Annotation(paragraph);
				pipelineSentenceSplitter.annotate(document);
				TextDocument paragraphDocument;
				List<CoreMap> coreMapSentences = document.get(SentencesAnnotation.class);
				int processingThreshold = 100;
				int offSet = 0;
				int counter = 0;
				String changedParagraph = "";
				for (CoreMap currentSentence : coreMapSentences) {
					edu.stanford.nlp.simple.Sentence countTokensSentence = new edu.stanford.nlp.simple.Sentence(
							currentSentence);
					// IF SENTENCE HAS MORE THAN 100 TOKENS, DO NOT PROCESS IT!
					if (countTokensSentence.length() > 100) {
						continue;
					}
					changedParagraph += " " + currentSentence.toString();
					counter++;
					if (counter >= processingThreshold) {
						paragraphDocument = new TextDocument(changedParagraph, pipelineDepParser);
						paragraphSentences = paragraphDocument
								.getSentences((offSet * processingThreshold) + paragraphOffset);
						locations.addAll(paragraphDocument.getLocations());
						people.addAll(paragraphDocument.getPeople());
						organizations.addAll(paragraphDocument.getOrganizations());
						paragraphRepMentionsSentences = paragraphDocument.getRepMentionsSentences();
						repMentions = mergeMaps(repMentions, paragraphDocument.getRepMentions());
						repMentionsSentences = mergeMapsInteger(repMentionsSentences, paragraphRepMentionsSentences);
						for (Sentence s : paragraphSentences) {
							sentences.add(s);
						}
						changedParagraph = "";
						counter = 0;
						offSet++;
					}
				}
				paragraphDocument = new TextDocument(changedParagraph, pipelineDepParser);
				paragraphSentences = paragraphDocument.getSentences(((offSet * processingThreshold) + paragraphOffset));
				locations.addAll(paragraphDocument.getLocations());
				people.addAll(paragraphDocument.getPeople());
				organizations.addAll(paragraphDocument.getOrganizations());
				paragraphRepMentionsSentences = paragraphDocument.getRepMentionsSentences();
				repMentions = mergeMaps(repMentions, paragraphDocument.getRepMentions());
				repMentionsSentences = mergeMapsInteger(repMentionsSentences, paragraphRepMentionsSentences);
				for (Sentence s : paragraphSentences) {
					sentences.add(s);
					paragraphOffset = sentences.size();
				}
			}
		}
	}

}
