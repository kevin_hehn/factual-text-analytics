package de.uni.mannheim.fta.processing;

/**
 * Class, representing an GigaWordsArticle
 * 
 * @author Kevin
 * 
 *
 */
public class RawGigaWordArticle {

	private String documentID;
	private String documentType;
	private String fullText;
	private String headline;
	private String dateline;

	public RawGigaWordArticle(String ID, String type, String full, String headline, String dateline) {
		this.documentID = ID;
		this.documentType = type;
		this.fullText = full;
		this.headline = headline;
		this.dateline = dateline;
	}

	public RawGigaWordArticle() {

	}

	public String getDocumentID() {
		return documentID;
	}

	public void setDocumentID(String documentID) {
		this.documentID = documentID;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public String getHeadline() {
		return headline;
	}

	public void setHeadline(String headline) {
		this.headline = headline;
	}

	public String getDateline() {
		return dateline;
	}

	public void setDateline(String dateline) {
		this.dateline = dateline;
	}

	@Override
	public String toString() {
		String output = "***********" + "ID: " + this.documentID + "Type: " + this.documentType + "Dateline:"
				+ this.dateline + "Headline:" + this.headline + "**** Full Text ****" + this.fullText;
		return output;
	}

}
