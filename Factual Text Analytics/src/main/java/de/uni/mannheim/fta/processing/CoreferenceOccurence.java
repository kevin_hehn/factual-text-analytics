package de.uni.mannheim.fta.processing;

/**
 * This class is used for the coreference annotation. It works as follows:
 * 
 * Let's say there is a article: "Donald Trump is president of the U.S. He lives
 * in the white house". In this example there would be one Coref Cluster,
 * namely: {Donald Trump; He} Stanford CoreNLP would assign i.E 1 as a Cluster
 * ID for this.
 * 
 * In this case we would have two CoreferenceOccurences. For each occurence, the
 * sentence number is stored, as well as the start- and endindex, to identify
 * the coref tokens later. There is also a representativeMention attribute,
 * which stores the "best" mention.
 * 
 * For this example, this would be: { sentenceNumber = 1, startIndex = 1,
 * endIndex = 3, clusterID = 1, representativeMention = "Donald Trump" }
 * 
 * @author Kevin
 *
 */
public class CoreferenceOccurence {
	private int sentenceNumber;
	private int startIndex;
	private int endIndex;
	private int clusterID;
	private String representativeMention;

	public String getRepresentativeMention() {
		return representativeMention;
	}

	public void setRepresentativeMention(String representativeMention) {
		this.representativeMention = representativeMention;
	}

	public int getSentenceNumber() {
		return sentenceNumber;
	}

	public void setSentenceNumber(int sentenceNumber) {
		this.sentenceNumber = sentenceNumber;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public int getEndIndex() {
		return endIndex;
	}

	public void setEndIndex(int endIndex) {
		this.endIndex = endIndex;
	}

	public int getClusterID() {
		return clusterID;
	}

	public void setClusterID(int clusterID) {
		this.clusterID = clusterID;
	}
}
