package de.uni.mannheim.fta.processing;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.wikiclean.WikiClean;
import org.wikiclean.WikiCleanBuilder;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

public class WikipediaSaxHandler implements ContentHandler {
	private String currentValue;
	private PrintWriter writer;
	private boolean inPage = false;
	private boolean inTitle = false;
	private boolean inText = false;
	private String importPath;
	StringBuilder sb = new StringBuilder();
	/**
	 * This method is called by the sax parser, if new Text is found
	 */
	public WikipediaSaxHandler(String input){
		super();
		this.importPath = input;
	}
	
	
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		currentValue = new String(ch, start, length);
		currentValue = currentValue.trim();
		sb.append(currentValue);

	}
	/**
	 * This method is called, if the SAX Parser finds an start tag.
	 */
	public void startElement(String uri, String localName, String qName,
			Attributes atts) throws SAXException {
		
		
		

		
	    if (localName.equals("page")) {
			inPage = true;
		}
		if (localName.equals("title")) {
			inTitle = true;
		}
		if(localName.equals("text")){
			inText = true;
		}


	}
	/**
	 * This method is called if the sax parser finds an end tag
	 */
	public void endElement(String uri, String localName, String qName)
			throws SAXException {    
		//There can be multiple documents in one file. Therefore every time the process finds a DOC end
		//tag it will add the current article to the ArrayList and will null the art variable afterwards
		if (localName.equals("page") && inPage == true) {
			inPage = false;
		}
		if(localName.equals("title") && inPage==true && inTitle == true){
			inTitle = false;
			try {
				writer = new PrintWriter(importPath +"\\"+ sb.toString()+".txt", "UTF-8");
			} catch (FileNotFoundException | UnsupportedEncodingException e) {
				System.out.println("FileNotFound or Unsupported Encoding");
				e.printStackTrace();
			}
		}
		if(localName.equals("text") && inPage==true && inTitle == true && inText == true ){
			inText = false;
			WikiClean cleaner =
				    new WikiCleanBuilder()
				        .withTitle(false)
				        .withFooter(false).build();
			String content = cleaner.clean(sb.toString());
			writer.write(content);
			writer.close();
		}
		
	}


	public void endDocument() throws SAXException {}
	public void endPrefixMapping(String prefix) throws SAXException {}
	public void ignorableWhitespace(char[] ch, int start, int length)
			throws SAXException {}
	public void processingInstruction(String target, String data)
			throws SAXException {}
	public void setDocumentLocator(Locator locator) {  }
	public void skippedEntity(String name) throws SAXException {}
	public void startDocument() throws SAXException {}
	public void startPrefixMapping(String prefix, String uri)
			throws SAXException {}
}
