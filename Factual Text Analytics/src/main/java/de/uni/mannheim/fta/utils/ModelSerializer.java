package de.uni.mannheim.fta.utils;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import avroschema.Sentence;

public class ModelSerializer extends JsonSerializer<List<Sentence>> {

    @Override
    public void serialize(List<Sentence> value, JsonGenerator jgen,
            SerializerProvider provider) throws IOException,
            JsonProcessingException {
        jgen.writeStartArray();
        for (Sentence model : value) {
            jgen.writeStartObject();
            jgen.writeObjectField("s_id", model);
            jgen.writeObjectField("sg", model);
            jgen.writeEndObject();    
        }
        jgen.writeEndArray();
    }

}