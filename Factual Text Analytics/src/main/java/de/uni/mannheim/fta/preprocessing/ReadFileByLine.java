package de.uni.mannheim.fta.preprocessing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

/**
 * Class used for splitting up Wikiforia results in chunks of 1000 articles each.
 * This is needed for debugging purposes and for better overview over the raw data.
 * 
 * @author Kevin
 *
 */
public class ReadFileByLine {

	/**
	 * Splits a given document in smaller documents containg 1000 articles each
	 * 
	 * @param inputFile
	 * @param outputDir
	 */
	public static void splitByLine(String inputFile, String outputDir) {
		try {
			//Get respective readers and writers
			Reader reader = (new InputStreamReader(new FileInputStream(inputFile), "UTF-8"));
			BufferedReader br = new BufferedReader(reader);
			String line;
			int filenamecounter = 0;
			Writer out = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(outputDir + "\\" + filenamecounter + ".xml"), "UTF8"));
			//Split after every 1000th article
			int counter = 1;
			while ((line = br.readLine()) != null) {
				line = line + "\n";
				out.append(line);
				if (counter > 1000) {
					filenamecounter++;
					out.flush();
					out.close();
					out = new BufferedWriter(new OutputStreamWriter(
							new FileOutputStream(outputDir + "\\" + filenamecounter + ".xml"), "UTF8"));
					System.out.println("Wrote 1000 lines");

					counter = 1;
				}
				counter++;
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
