package de.uni.mannheim.fta.processing;

/**
 * Represents a Wikipedia Article, needed for the importing/parsing.
 * @author Kevin
 *
 */
public class WikipediaArticle implements java.io.Serializable  {

	private static final long serialVersionUID = 6650672944999922943L;
	/*
	 * Attributes - Representing the structure of a wikipedia article as seen in the dump.xml
	 */
	private String title;
	private String ns;
	private String ns_id;
	private String id;
	public String getNs_id() {
		return ns_id;
	}
	public void setNs_id(String ns_id) {
		this.ns_id = ns_id;
	}
	private String revision_id;
	private String revision_parentid;
	private String revision_timestamp;
	private String revision_contributor_username;
	private String revision_contributor_id;
	private String revision_minor;
	private String revision_comment;
	private String revision_model;
	private String revision_format;
	private String revision_text;
	private String revision_sha1;
	/*
	 * Getter & Setter methods
	 */
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getNs() {
		return ns;
	}
	public void setNs(String ns) {
		this.ns = ns;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRevision_id() {
		return revision_id;
	}
	public void setRevision_id(String revision_id) {
		this.revision_id = revision_id;
	}
	public String getRevision_parentid() {
		return revision_parentid;
	}
	public void setRevision_parentid(String revision_parentid) {
		this.revision_parentid = revision_parentid;
	}
	public String getRevision_timestamp() {
		return revision_timestamp;
	}
	public void setRevision_timestamp(String revision_timestamp) {
		this.revision_timestamp = revision_timestamp;
	}
	public String getRevision_contributor_username() {
		return revision_contributor_username;
	}
	public void setRevision_contributor_username(String revision_contributor_username) {
		this.revision_contributor_username = revision_contributor_username;
	}
	public String getRevision_contributor_id() {
		return revision_contributor_id;
	}
	public void setRevision_contributor_id(String revision_contributor_id) {
		this.revision_contributor_id = revision_contributor_id;
	}
	public String getRevision_minor() {
		return revision_minor;
	}
	public void setRevision_minor(String revision_minor) {
		this.revision_minor = revision_minor;
	}
	public String getRevision_comment() {
		return revision_comment;
	}
	public void setRevision_comment(String revision_comment) {
		this.revision_comment = revision_comment;
	}
	public String getRevision_model() {
		return revision_model;
	}
	public void setRevision_model(String revision_model) {
		this.revision_model = revision_model;
	}
	public String getRevision_format() {
		return revision_format;
	}
	public void setRevision_format(String revision_format) {
		this.revision_format = revision_format;
	}
	public String getRevision_text() {
		return revision_text;
	}
	public void setRevision_text(String revision_text) {
		this.revision_text = revision_text;
	}
	public String getRevision_sha1() {
		return revision_sha1;
	}
	public void setRevision_sha1(String revision_sha1) {
		this.revision_sha1 = revision_sha1;
	}
}
